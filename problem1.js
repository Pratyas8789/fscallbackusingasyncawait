
const fs = require('fs')
const path = require('path')

function createJsonDirectory(folderName) {
    return new Promise((resolve, reject) => {
        fs.access(folderName, error => {
            if (error) {
                console.log("First time folder created");
                fs.mkdir(folderName, error => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve("Directory created successfully")
                    }
                })
            } else {
                resolve("Already folder created");
            }
        })
    })
}


function createAndDeleteFolderAndFiles(folderPath, num) {
    return new Promise((resolve, reject) => {
        for (let i = 0; i < num; i++) {
            let fileName = `file${i}` + '.json'
            fs.writeFile(path.join(folderPath, fileName), 'w', (error) => {
                if (error) {
                    reject(error)
                } else {
                    console.log(fileName + " " + "created");
                    fs.unlink(path.join(folderPath, fileName), (error) => {
                        if (error) {
                            reject(error)
                        } else {
                            console.log(fileName + " " + "deleted");
                            resolve();
                        }
                    })
                }
            })
        }

    })
}




module.exports.createAndDeleteFolderAndFiles = createAndDeleteFolderAndFiles;
module.exports.createJsonDirectory = createJsonDirectory;
