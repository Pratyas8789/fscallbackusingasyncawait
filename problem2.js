const fs = require('fs');

function readingFunction(fileData) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileData, 'utf8', (error, data) => {
            if (error) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    })
}

function convertUpperCase(data) {
    data = data.toUpperCase();
    return new Promise((resolve, reject) => {
        fs.writeFile("upperCase.txt", data, error => {
            if (error) {
                reject(error);
            } else {
                fs.writeFile('filenames.txt', "upperCase.txt", (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        console.log("upperCase.txt created successfully");
                        resolve(data);
                    }
                })
            }
        });
    })
}

function convertLowerCase(data) {
    data = data.toLowerCase().split('. ').join('.\n')
    return new Promise((resolve, reject) => {
        fs.writeFile("lowerCase.txt", data, error => {
            if (error) {
                reject(error);
            } else {
                fs.appendFile("filenames.txt", "\n" + "lowerCase.txt", error => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        console.log("lowerCase.txt created successfully");
                        resolve(data);
                    }
                })

            }
        })
    })
}

function sort(data) {
    return new Promise((resolve, reject) => {
        fs.readFile("upperCase.txt", 'utf8', (error, upperFileData) => {
            if (error) {
                reject(error);
            }
            else {
                let allData = upperFileData + data
                allData = allData.split(" ").sort().toString()
                fs.writeFile("sortFile.txt", allData, error => {
                    if (error) {
                        reject(error);
                    } else {
                        fs.appendFile("filenames.txt", "\n" + "sortFile.txt", error => {
                            if (error) {
                                reject(error);
                            }
                            else {
                                console.log("sortFile.txt created successfully");
                                resolve(allData);
                            }
                        })
                    }
                })
            }
        });
    })


}

function deleteFile() {
    return new Promise((resolve, reject) => {
        fs.readFile('filenames.txt', 'utf8', (error, data) => {
            if (error) {
                reject(error);
            }
            else {
                let dataarray = data.split('\n')
                const deletePromises = dataarray.map((ele) => {
                    return new Promise((resolve, reject) => {
                        fs.unlink(ele, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(`\n${ele} deleted successfully`)
                            }
                        })
                    })
                })
                Promise.all(deletePromises)
                    .then((resolvedArray) => {
                        resolve(resolvedArray);
                    })
                    .catch((error) => {
                        reject(error);
                    })
            }
        });
    })
}


module.exports.readingFunction = readingFunction;
module.exports.convertUpperCase = convertUpperCase;
module.exports.convertLowerCase = convertLowerCase;
module.exports.sort = sort;
module.exports.deleteFile = deleteFile;
