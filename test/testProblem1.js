const functions = require('../problem1')
const folderName = './JSONfolder'


async function calledJsonDirectory() {
    try {
        const response = await functions.createJsonDirectory(folderName)
        console.log(response);
        await functions.createAndDeleteFolderAndFiles(folderName, 5);
    }
    catch (error) {
        console.error(error);
    }

}
calledJsonDirectory()

