const problem2 = require('../problem2');
const path = require('path');

const fileData = "lipsum.txt";

async function readingDirectory() {
    try {
        const response1 = await problem2.readingFunction(fileData);
        const response2 = await problem2.convertUpperCase(response1);
        const response3 = await problem2.convertLowerCase(response2);
        const response4 = await problem2.sort(response3);
        const response5 = await problem2.deleteFile(response4);
        console.log(...response5);
    }
    catch (error) {
        console.error(error);
    }
}

readingDirectory();